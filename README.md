[![Coverage Status](https://coveralls.io/repos/bitbucket/atlassian/confluence-web-components/badge.svg?branch=master)](https://coveralls.io/bitbucket/atlassian/confluence-web-components?branch=master)
## Installation

1. Git clone the repository.


2. Run ```composer install```


3. Make sure that you set the access right for cake console ```chmod +x bin/cake```


4. You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:
```bin/cake server -p 8765```

5. Then visit `http://localhost:8765/twitter/index` to see the twitter api page.

## Run Poker Shell

inside the root directory, you run the poker app by entering the following command in terminal.

```bin/cake Poker```

This will generate a hand and determine wether it is straight or straight flush.
e.g. 
`
--- Generating Poker Hand ---
3h
9d
3s
jh
2d
--- No Straight Or Straight Flush Found ---`

# Unit Test

The unit tests can be executed by running the following (in root directory).

```./vender/bin/phpunit ./tests/TestCase/Poker/BusinessLogicPokerClassTest.php```
```./vender/bin/phpunit ./tests/TestCase/Shell/PokerShellTest.php```

## Run Twitter Api

1. Run a webserver (or you can use the built in one cake provides you)
```bin/cake server -p 8765```

2. Go to the index page located here.
```http://localhost:8765/twitter/index```

3. If you want to free search, enter some text in the input and click `search`
You can also go back to the index page by clicking `back to Index`

I have wrote integration and functional tests.
for the functional test, I mocked the get call which hits the twitter API.

The unit tests can be executed by running the following (in root directory).

# Unit Test

```./vender/bin/phpunit ./tests/TestCase/Controller/TwitterControllerTest.php```
```./vender/bin/phpunit ./tests/TestCase/Pressentation/PresentationLayerTest.php```
```./vender/bin/phpunit ./tests/TestCase/TwitterApi/Functional/FunctionalTwitterApiClientTest.php```

# Code Coverage
I have added the badge to determine the percentage of code that is covered by unit test.
For a better report, please look at the 
```coverage/index.html```
. It's rougly 95%.

If you have any questions or issues, please let me know and I will address them.


# Run All Unit Tests

if you want to run all the unit tests, just use:
```./vender/bin/phpunit ./tests```


Let me know if you have any issues and I wil address it.

Thanks





