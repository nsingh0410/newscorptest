<?php

namespace App\Shell;

use App\Poker\BusinessLogicPokerClass;
use Cake\Console\ConsoleIo;
use Cake\Console\Shell;
use Cake\ORM\Locator\LocatorInterface;

/**
 * Poker shell command.
 */
class PokerShell extends Shell
{
    /**
     * @var BusinessLogicPokerClass App\Poker\BusinessLogicPokerClass
     */
    private $businessLogicPokerClass;

    /**
     * PokerShell constructor.
     *
     * @param ConsoleIo|null $io The Inputs and Outputs
     * @param LocatorInterface|null $locator The Table Locator
     */
    public function __construct(ConsoleIo $io = null, LocatorInterface $locator = null)
    {
        $this->businessLogicPokerClass = new BusinessLogicPokerClass();
        parent::__construct($io, $locator);
    }

    /**
     * This is the main driver of the poker application.
     *
     * @return bool
     */
    public function main()
    {
        $this->out('--- Generating Poker Hand ---');
        $hand = $this->businessLogicPokerClass->generateAndPrintHand();
        $this->out($hand);
        $result = $this->businessLogicPokerClass->isStraightOrStraightFlush($hand);

        if ($result === '') {
            $this->out('--- No Straight Or Straight Flush Found ---');
        }

        if ($result === 's') {
            $this->out('--- Straight Found ---');
        }

        if ($result === 'sf') {
            $this->out('--- Straight Flush Found ---');
        }

        return true;
    }
}
