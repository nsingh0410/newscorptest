<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;

$this->layout = false;
?>
<!DOCTYPE html>
<html>
<body class="home">

<header class="row">
    <div class="header-title">
        <h1>Twitter Search Results</h1>
    </div>

    <?php
    echo $this->Form->create('twitter-form', ['url' => ['action' => 'search']]);
    echo $this->Form->text('search');
    echo $this->Form->button(__('Search'));
    echo $this->Form->end();
    ?>

    <?php
        foreach ($tweetText As $tweet)
        {
        ?>
        <div class="row bg-info">
            <div class="columns large-6">
                <?php
                echo '</br>';
                echo $tweet;
                echo '</br>';
                ?>
            </div>
        </div>
        <?php
        }

        echo $this->Html->div('col-md-4', $this->Html->link(
                            __('Back to Index'),
                            ['action' => 'Index'],
                            ['class' => 'btn btn-primary btn-lg btn-block']
                        ));
        ?>
</header>
</body>
</html>
