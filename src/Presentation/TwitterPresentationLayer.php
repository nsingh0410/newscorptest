<?php
namespace App\Presentation;

use App\TwitterApi\TwitterApiBusinessLogicLayer;

/**
 * @property TwitterApiBusinessLogicLayer businessLogicLayer
 */
class TwitterPresentationLayer
{
    public function __construct()
    {
        $this->businessLogicLayer = new TwitterApiBusinessLogicLayer();
    }

    /**
     * Gets the twitter timeline object in array format
     */
    public function getTwitterTimelineToText(array $tweets): array
    {
        $tweets = $this->businessLogicLayer->getValidTweets($tweets);
        $tweetText = [];

        // get the tweet text to output to the screen
        foreach ($tweets as $tweet)
        {
                $tweetText[] = $tweet->text;
        }

        return $tweetText;
    }

    /**
     * Gets the twitter search object in array format
     */
    public function getTwitterSearchToText(object $tweets): array
    {
        $tweetText = [];
        // get the tweet text to output to the screen
        foreach ($tweets as $object => $tweet) {
            foreach ($tweet as $tweet) {
                if (isset($tweet->text) === true) {
                    $tweetText[] = $tweet->text;
                }
            }
        }

        return $tweetText;
    }
}
