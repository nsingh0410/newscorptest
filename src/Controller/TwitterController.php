<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Presentation\TwitterPresentationLayer;
use App\TwitterApi\TwitterApiClient;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

/**
 * Twitter Controller
 *
 */
class TwitterController extends AppController
{
    /**
     * @var TwitterApiClient App\TwitterApi\TwitterApiClient
     */
    private $twitterClient;
    /**
     * @var array
     */
    private $tweets;
    /**
     * @var TwitterPresentationLayer App\Presentation\TwitterPresentationLayer
     */
    private $presentationLayer;

    /**
     * TwitterController constructor.
     * @param ServerRequest|null $request
     * @param Response|null $response
     * @param null $name
     * @param null $eventManager
     * @param null $components
     * @throws \App\Exception\TwitterException
     */
    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, $eventManager = null, $components = null)
    {
        $this->twitterClient = new TwitterApiClient();
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->presentationLayer = new TwitterPresentationLayer();
    }

    /**
     * Main twitter preview page.
     *
     * @return void
     */
    public function index(): void
    {
        // Gets all the latest tweets (defaults to 200)
        $this->tweets = $this->twitterClient->get();
        $tweetText = $this->presentationLayer->getTwitterTimelineToText($this->tweets);

        $this->set(compact('tweetText'));
    }

    /**
     * The main
     */
    public function search()
    {
        $tweetText = [];
        $searchInput = $this->request->getData('search');
        if (empty($searchInput)) {
            $this->set(compact('tweetText'));

            return;
        }

        $this->twitterClient->setApiPath('search/tweets');
        $this->twitterClient->setApiOptions(['q' => $searchInput]);
        $this->tweets = $this->twitterClient->get();

        $tweetText = $this->presentationLayer->getTwitterSearchToText($this->tweets);

        $this->set(compact('tweetText'));
    }
}
