<?php


namespace App\TwitterApi;


class TwitterApiBusinessLogicLayer extends TwitterApiClient
{
    /**
     * @var array Collection of tweets
     */
    protected $tweets = [];

    /**
     * Filters the collection of tweets.
     * @param array $tweets
     * @return array
     */
    public function getValidTweets(array $tweets): array
    {
        foreach ($tweets as $tweet)
        {
            // Get by Hashtag
            if (empty($tweet->entities->hashtags) === false)
            {
                $this->tweets[] = $tweet;
            }

            // Get by Mentions
            if (empty($tweet->entities->user_mentions) === false)
            {
                foreach ($tweet->entities->user_mentions As $mention)
                {
                    if ($mention->screen_name === 'taste_team')
                    {
                        $this->tweets[] = $tweet;
                    }
                }
            }
        }

        return $this->tweets;
    }

    /**
     * @return array
     */
    public function getTweets(): array
    {
        return $this->tweets;
    }

    /**
     * @param array $tweets
     */
    public function setTweets($tweets)
    {
        $this->tweets = $tweets;
    }
}