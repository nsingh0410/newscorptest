<?php

namespace App\TwitterApi;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Exception\TwitterException;
use Cake\Core\Configure;

class TwitterApiClient
{
    protected $apiKey;
    protected $apiSecretKey;
    protected $accessToken;
    protected $accessTokenSecret;

    /**
     * @var TwitterOAuth Abraham\TwitterOAuth\TwitterOAuth The twitter OAuth connection.
     */
    protected $connection;

    /**
     * @var string Path we want to hit the api
     */
    public $apiPath = 'statuses/home_timeline';

    /**
     * @var array Options that we want
     */
    public $apiOptions = ['count' => 200, 'exclude_replies' => true, 'include_entities' => true];

    /**
     * TwitterApiClient constructor.
     * @param array $options
     * @throws TwitterException
     */
    public function __construct(array $options = [])
    {
        $config = Configure::read('TwitterApi');
        $this->apiKey = $config['apiKey'];
        $this->apiSecretKey = $config['apiSecretKey'];
        $this->accessToken = $config['accessToken'];
        $this->accessTokenSecret = $config['accessTokenSecret'];

        if (isset($config['connectApi']) && $config['connectApi'] === true) {
            $this->setupConnection();
        }
    }

    /**
     * Set
     */
    public function setupConnection()
    {
        $this->connection = new TwitterOAuth($this->apiKey, $this->apiSecretKey, $this->accessToken, $this->accessTokenSecret);
        $content = $this->connection->get('account/verify_credentials');
    }

    /**
     * Performs a get operation on the twitter API.
     * @param array $options
     * @return array|object
     */
    public function get(array $options = [])
    {
        $connection = $this->getConnection();
        $result = $connection->get($this->apiPath, $this->apiOptions);

        return $result;
    }

    /**
     * @return TwitterOAuth
     */
    public function getConnection(): TwitterOAuth
    {
        return $this->connection;
    }

    /**
     * @param TwitterOAuth $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function getApiOptions(): array
    {
        return $this->apiOptions;
    }

    /**
     * @param array $apiOptions
     */
    public function setApiOptions($apiOptions)
    {
        $this->apiOptions = $apiOptions;
    }

    /**
     * @return string
     */
    public function getApiPath(): string
    {
        return $this->apiPath;
    }

    /**
     * @param string $apiPath
     */
    public function setApiPath($apiPath)
    {
        $this->apiPath = $apiPath;
    }

    /**
     * @return mixed
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getApiSecretKey(): string
    {
        return $this->apiSecretKey;
    }

    /**
     * @param mixed $apiSecretKey
     */
    public function setApiSecretKey($apiSecretKey)
    {
        $this->apiSecretKey = $apiSecretKey;
    }

    /**
     * @return mixed
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getAccessTokenSecret(): string
    {
        return $this->accessTokenSecret;
    }

    /**
     * @param mixed $accessTokenSecret
     */
    public function setAccessTokenSecret($accessTokenSecret)
    {
        $this->accessTokenSecret = $accessTokenSecret;
    }
}
