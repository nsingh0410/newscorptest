<?php

namespace App\Poker;

use App\Constants\PokerConstants;

class BusinessLogicPokerClass
{
    /**
     * @var array A Single Random Card
     */
    protected $randomCard;

    /**
     * @var int The total hand size
     */
    protected $handCount = 5;

    /**
     * @var array A collection of all the cards in a hand
     */
    protected $hand = [];

    /**
     * Generates and prints the random hand.
     *
     * @return array
     */
    public function generateAndPrintHand(): array
    {
        // build the hand with random cards.
        for ($x = 1; $x <= $this->handCount; $x++)
        {
            $this->hand[] = $this->randomCard();
        }

        return $this->hand;
    }

    /**
     * Generates a random card.
     *
     * @return array
     */
    public function randomCard(): string
    {
        $randomCaseIndex = array_rand(PokerConstants::CARD_CASES);
        $randomNumberIndex = array_rand(PokerConstants::CARD_NUMBERS);
        $randomCase = PokerConstants::CARD_CASES[$randomCaseIndex];
        $randomNumber = PokerConstants::CARD_NUMBERS[$randomNumberIndex];
        // If the number is high card override the cardNumber for output.
        if (isset(PokerConstants::HIGH_CARDS[$randomNumber])) {
            $randomNumber = PokerConstants::HIGH_CARDS[$randomNumber];
        }

        $this->randomCard = $randomNumber . $randomCase;

        // If we have a duplicate, regenerate card.
        if ($this->hasDuplicateCard() === true)
        {
            $this->randomCard();
        }

        return $this->randomCard;
    }

    /**
     * Determines if the card is a duplicate.
     * @return bool
     */
    public function hasDuplicateCard()
    {
        // If we generate a random card that already exists.
        if (in_array($this->getRandomCard(), $this->hand) === true) {

            return true;
        }

        return false;
    }

    /**
     * Determines whether we have a straight or straight flush based on hand.
     * @param array $c A collection of all the cards
     * @return string
     */
    public function isStraightOrStraightFlush(array $c): string
    {
        for($i=0;$i<count($c);$i++){$a=str_split($c[$i],strlen($c[$i])-1);$b[]=(int)$a[0];$u[]=$a[1];}sort($b);sort($u);$b[4]-$b[0]==4?$s=1:$s=0;$u[0]==$u[4]?$f=1:$f=0;$s&&!$f?$r='s':($s&&$f?$r='sf':$r='');return$r;
    }

    /**
     * Gets the hand count.
     * @return int
     */
    public function getHandCount(): int
    {
        return $this->handCount;
    }

    /**
     * Sets the hand count.
     * @param int $handCount The amount of cards we want in the hand.
     * @return void
     */
    public function setHandCount($handCount)
    {
        $this->handCount = $handCount;
    }

    /**
     * Gets the hand.
     * @return array
     */
    public function getHand(): array
    {
        return $this->hand;
    }

    /**
     * Sets the hand.
     * @param array $hand The hand collection.
     * @return void
     */
    public function setHand($hand)
    {
        $this->hand = $hand;
    }

    /**
     * @return array
     */
    public function getRandomCard()
    {
        return $this->randomCard;
    }

    /**
     * @param array $randomCard
     */
    public function setRandomCard($randomCard)
    {
        $this->randomCard = $randomCard;
    }
}
