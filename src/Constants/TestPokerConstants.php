<?php

namespace App\Constants;

class TestPokerConstants extends PokerConstants
{
    public const STRAIGHT = ['7c', '8d', '9h', '10s', '11d'];

    public const STRAIGHT_FLUSH = ['2s', '3s', '4s', '5s', '6s'];

    public const NO_STRAIGHT_OR_STRAIGHT_FLUSH = ['2s', '3s', '4s', '7d', '5s'];

    public const ALL_CARDS = [
        '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', '11s', '12s', '13s', '14s',
        '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', '11d', '12d', '13d', '14d',
        '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', '11c', '12c', '13c', '14c',
        '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', '11h', '12h', '13h', '14h'
    ];
}
