<?php


namespace App\Constants;

class PokerConstants
{
    public const CARD_NUMBERS = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
    public const CARD_CASES = ['s', 'c', 'h', 'd'];
    public const HIGH_CARDS = [11 => 'j', 12 => 'q', 13 => 'k', 14 => 'a'];
}
