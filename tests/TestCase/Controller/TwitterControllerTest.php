<?php

namespace App\Test\TestCase\Controller;

use App\Controller\TwitterController;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\TwitterController Test Case
 *
 * @uses \App\Controller\TwitterController
 */
class TwitterControllerTest extends TestCase
{
    use IntegrationTestTrait;

    public $route = ['controller' => 'Twitter', 'action' => 'index'];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get($this->route);
        $this->assertResponseContains('twitter');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testSearch()
    {

        Configure::write('TwitterApi.connectApi', false);
        $route = ['controller' => 'Twitter', 'action' => 'search'];
        $data = ['search' => ''];
        $this->enableCsrfToken();
        $this->post($route, $data);
        $this->assertResponseOk();

        $route = ['controller' => 'Twitter', 'action' => 'search'];
        $data = ['search' => 'q'];
        $this->enableCsrfToken();
        $this->post($route, $data);
    }
}
