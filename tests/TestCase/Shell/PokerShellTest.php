<?php

namespace App\Test\TestCase\Shell;

use App\Shell\PokerShell;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Shell\PokerShell Test Case
 */
class PokerShellTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \App\Shell\PokerShell
     */
    public $Poker;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();
        $this->Poker = new PokerShell($this->io);
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->assertTrue($this->Poker->main());
    }
}
